<?php

namespace Drupal\container_dumper\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\XmlDumper;
use Symfony\Component\Filesystem\Filesystem;

/**
 * A compiler pass dumping the container to an XML file at a specific path.
 */
class DumpCompilerPass implements CompilerPassInterface {

  /**
   * The path at which the container file will be dumped.
   *
   * @var string|null
   */
  protected $path;

  /**
   * Constructs a new compiler pass.
   *
   * @param string|null $path
   *   The path at which the container file will be dumped.
   */
  public function __construct(?string $path) {
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    if (empty($this->path)) {
      return;
    }

    $filename = basename($this->path);
    $dirname = DRUPAL_ROOT . DIRECTORY_SEPARATOR . trim(dirname($this->path), DIRECTORY_SEPARATOR);

    if (!is_dir($dirname) && !mkdir($dirname, 0775, TRUE) && !is_dir($dirname)) {
      throw new \RuntimeException(sprintf('Directory "%s" was not created', $dirname));
    }

    $dumper = new XmlDumper($container);
    $filesystem = new Filesystem();

    $filesystem->dumpFile($dirname . DIRECTORY_SEPARATOR . $filename, $dumper->dump());
  }

}
